extern crate configure_me;

fn main() {
    configure_me::build_script_auto().unwrap_or_exit();
}